import User from '../models/User';

export const insertUser = async (email) => {
    let user = await User.findOne({email});
    if(!user) {
        user = await User.create({email})
    }
    return user;
}