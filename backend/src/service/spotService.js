import Spot from '../models/Spot';
import User from '../models/User';


export const findSpots = async (tech) => {
    const spots = await Spot.find({techs: tech});
    return spots;
}

export const insertSpot = async (user_id, thumbnail, company, techs, price) => {
    const user = await User.findById(user_id);
    if(!user) {
        return null;
    }
    const spot = await Spot.create( {
        user: user_id,
        thumbnail,
        company,
        techs: techs.split(",").map(tech => tech.trim()),
        price
    })
    return spot;
}