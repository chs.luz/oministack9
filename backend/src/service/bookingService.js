import Booking from '../models/Booking';

export const insertBooking = async (date, user, spot) => {
    const booking = await Booking.create( {
        date,
        user,
        spot
    })
    await booking.populate('user').populate('spot').execPopulate()
    return booking;
}