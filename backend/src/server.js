import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import path from 'path';
import socketIo from 'socket.io';
import http from 'http';

import Routes from './Routes';

const app = express();
const server = http.Server(app);
const io = socketIo(server);

mongoose.connect('mongodb+srv://henrique:281_Henri@cluster0-46nax.mongodb.net/ominiStack9?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const connectedUsers = {};
io.on('connection',socket => {
    const { user_id } = socket.handshake.query;
    connectedUsers[user_id] = socket.id;
})

app.use((req,res,next) => {
    req.io = io;
    req.connectedUsers = connectedUsers;
    return next();
})

app.use(express.json())
app.use(cors());
app.use('/files',express.static(path.resolve(__dirname,"..","uploads")))
app.use(Routes);

server.listen(3333);