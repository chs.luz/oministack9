import { insertBooking } from '../service/bookingService';

export const store = async (req, res ) => {
    const { user_id } = req.headers;
    const { spot_id} = req.params;
    const { date } = req.body;
    let booking = await insertBooking(date,user_id,spot_id);
    const ownerSocket = req.connectedUsers[booking.spot.user];
    if(ownerSocket) {
        req.io.to(ownerSocket).emit('booking_request',booking);
    }
    return res.json(booking);
}