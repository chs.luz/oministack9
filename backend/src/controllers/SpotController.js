import { insertSpot, findSpots } from '../service/spotService';

export const index = async (req, res ) => {
    const { tech } = req.query;
    let spots = await findSpots(tech);
    return res.json(spots);
}

export const store = async (req,res) => {
    const { filename } = req.file;
    const { company, techs, price } = req.body;
    const { user_id} = req.headers;
    let spot = await insertSpot(user_id, filename, company,techs, price)
    if(!spot)  {
        return res.status(400).json({error: 'User doenst exist'})
    }
    return res.json(spot)
}