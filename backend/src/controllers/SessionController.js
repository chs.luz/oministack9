import { insertUser } from '../service/sessionService';

export const store = async (req,res) => {
    const { email } = req.body;
    let user = await insertUser(email);
    res.json(user)
}