import express from 'express';
import multer from 'multer';
import { store as storeSession } from './controllers/SessionController';
import { store as storeSpots, index as getSpots } from './controllers/SpotController';
import { show } from './controllers/DashboardController';
import { store as storeBooking} from './controllers/BookingController';
import { store as approve} from './controllers/ApproveController';
import { store as reject} from './controllers/RejectController';

import UploadConfig from './config/upload';
const upload = multer(UploadConfig);
const routes = express.Router();

routes.post("/sessions", storeSession)
routes.post("/spots", upload.single('thumbnail'), storeSpots)
routes.get("/spots", getSpots)
routes.get("/dashboard", show);
routes.get("/spots/:spot_id/bookings",storeBooking);
routes.post("/bookings/:booking_id/approvals",approve);
routes.post("/bookings/:booking_id/rejections",reject)

export default routes;