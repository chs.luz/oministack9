import React, { useState, useMemo } from 'react';
import camera from '../../assets/camera.svg';
import api from '../../services/api';
import './styles.css';


export default function New({ history }) {

    const [company, setCompany] = useState('');
    const [techs, setTechs] = useState('');
    const [price, setPrice] = useState('');
    const [thumbnail, setThumbnail] = useState(null);

    const preview = useMemo(() => {
        return thumbnail ? URL.createObjectURL(thumbnail) : null
    },[thumbnail])

    async function handleSubmit(e) {
        e.preventDefault()
        const user_id = localStorage.getItem('user')
        const data = new FormData();
        data.append('thumbnail',thumbnail);
        data.append('company',company)
        data.append('price',price)
        data.append('techs',techs)
        const response = await api.post('/spots',data, {headers: {user_id}})
        console.log(response.data)
        history.push('/dashboard')
    }
    return (
        <form onSubmit={handleSubmit}>
            <label className={thumbnail ? 'has-thumbnail' : ''} id="thumbnail" style={{backgroundImage: `url(${preview})`}}>
                <input type="file" onChange={event => setThumbnail(event.target.files[0])} />
                <img src={camera} alt="Select a file" />
            </label>
            <label htmlFor="company"> COMPANY *</label>
            <input
                id="company"
                placeholder="Sua empresa incrível"
                value={company}
                onChange={e => setCompany(e.target.value)}
            />
            <label htmlFor="techs"> TECNOLOGIAS * <span>(separadas por virgulas)</span></label>
            <input
                id="techs"
                placeholder="Tecnologias favoritas"
                value={techs}
                onChange={e => setTechs(e.target.value)}
            />

            <label htmlFor="price"> VALOR DA DIARIA <span>(Em branco para gratuito)</span></label>
            <input
                id="price"
                placeholder="Preço"
                value={price}
                onChange={e => setPrice(e.target.value)}
            />
            <button type="submit" className="btn">Cadastrar</button>
        </form>
    )
}